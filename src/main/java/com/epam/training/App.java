package com.epam.training;

import com.epam.training.view.View;

import java.io.IOException;

public class App {
    public static void main(String[] args) {
        new View().run();
    }
}
