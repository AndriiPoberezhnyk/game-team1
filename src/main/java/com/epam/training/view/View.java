package com.epam.training.view;

import com.epam.training.controller.PlayerController;
import com.epam.training.model.enemy.Monster;
import com.epam.training.model.item.ArmorSet;
import sun.security.krb5.internal.crypto.Des;

import java.io.IOException;
import java.util.*;

public class View {
    private PlayerController controller;
    private List<String> menu;
    private Map<String, Printable> methodsMenu;
    private Scanner scanner;

    public View() {
        controller = null;
        scanner = new Scanner(System.in);
        menu = new LinkedList<>();
        methodsMenu = new LinkedHashMap<>();
    }

    public void run() {
        String option;
        Design.printCastle();
        generateMainMenu();
        showMenu();
        option = scanner.nextLine();
        if (option.equals("1")) {
            controller = new PlayerController();
            startGame();
        } else if(option.equals("2")){
            return;
        }
    }

    private void generateMainMenu() {
        menu.clear();
        menu.add("1 - Start game");
        menu.add("2 - Exit");
    }

    private void generatePlayerMenu() {
        menu.clear();
        methodsMenu.clear();
        int index = 1;
        if (controller.hasNextLocation()
                && !controller.hasMonster()) {
            menu.add(index + " - move into the right room -->");
            methodsMenu.put("" + index++, this::pressButtonGoToNextLocation);
        }
        if (controller.hasPreviousLocation()) {
            menu.add(index + " - <-- move into the left room ");
            methodsMenu.put("" + index++, this::pressButtonGoToPreviousLocation);
        }
        if (controller.hasStairs() && controller.hasUpperFloor()
                && controller.canMoveUp()  && !controller.hasMonster()) {
            menu.add(index + " - go upstairs");
            methodsMenu.put("" + index++, this::pressButtonGoUpstairs);
        }
        if (controller.hasStairs() && controller.hasLowerFloor()
                && !controller.hasMonster()) {
            menu.add(index + " - go downstairs");
            methodsMenu.put("" + index++, this::pressButtonGoDownstairs);
        }
        if (!controller.isChecked()
                && !controller.hasMonster()) {
            menu.add(index + " - search the location for something useful");
            methodsMenu.put("" + index++, this::pressButtonSearch);
        }
        if (controller.hasMonster()) {
            menu.add(index + " - fight with the monster");
            methodsMenu.put("" + index, this::pressButtonFight);
        }
    }

    private void generateFightMenu(){
        menu.clear();
        methodsMenu.clear();
        int index = 1;
        menu.add(index + " - attack the monster");
        methodsMenu.put("" + index++, this::pressButtonAttack);
        if(controller.hasNextLocation()) {
            menu.add(index + " - run to the right");
            methodsMenu.put("" + index++, this::pressButtonRunForward);
        }
        if(controller.hasPreviousLocation()) {
            menu.add(index + " - run to the left");
            methodsMenu.put("" + index, this::pressButtonRunBack);
        }
        if(controller.hasLowerFloor()) {
            menu.add(index + " - run downstairs");
            methodsMenu.put("" + index, this::pressButtonRunDownstairs);
        }
    }

    private void startGame() {
        String option;
        do {
            if(!controller.isPlayerLive()){
                Design.printIfDead();
                return;
            }
            if (controller.isLocationsEmpty()){
                printFinalScene();
                return;
            }
            Design.locationPrint();
            Design.heroPrint(controller);
            Design.printPlayerInfo(controller.getPlayer());
            generatePlayerMenu();
            printCurrentPosition();
            showMenu();
            option = scanner.nextLine();
            try {
                methodsMenu.get(option).print();
            } catch (Exception e) {
                System.out.println("Wrong input");
            }
        } while (!option.equals("suicide"));
    }

    private void printCurrentPosition() {
        System.out.println("\nYou are in " + (controller.getCurrentLocation() + 1) +
                " location at " + (controller.getCurrentFloor() + 1) + " floor");
    }

    private void pressButtonFight() {
        String option;
        generateFightMenu();
        do{
            Design.locationPrint();
            Design.heroPrint(controller);
            if (!controller.hasMonster())
                return;
            Design.printPlayerInfo(controller.getPlayer());
            System.out.print("\t\t\t\t\t");
            Design.printMonsterInfo(controller.getMonster());
            showFightMenu();
            option = scanner.nextLine();
            try{
                methodsMenu.get(option).print();
            }catch(Exception e){
                System.out.println("Wrong input");
            }
            if(!controller.isPlayerLive()){
                return;
            }
        }while(option.equals("1"));
    }

    private void showFightMenu() {
        System.out.println("Choose what to do: ");
        for(String option : menu){
            System.out.println(option);
        }
        System.out.print(">>> ");
    }

    private void showChangeItemMenu() {
        System.out.println("You have found " +
                controller.getItemName());
        if (controller.getItemName().matches("Drug(.*)")){
            System.out.println("Do you want to use it?");
        }else{
            System.out.println("Do you want to change your" +
                controller.getPlayerItemName() + "?");
        }
        System.out.print("1 - yes\n2 - no\n" +
                "Type your choice: ");
    }

    private void pressButtonAttack() {
        controller.attack();
    }


    private void pressButtonRunBack() {
        controller.runBack();
    }

    private void pressButtonRunDownstairs() {
        controller.runDownstairs();
    }

    private void pressButtonRunForward() {
        controller.runForward();
    }

    private void pressButtonGoToNextLocation() {
        controller.goToNextLocation();
    }

    private void pressButtonGoToPreviousLocation() {
        controller.goToPreviousLocation();
    }

    private void pressButtonGoUpstairs() {
        controller.goToNextFloor();
    }

    private void pressButtonGoDownstairs() {
        controller.goToPreviousFloor();
    }

    private void pressButtonSearch() {
        boolean searchResult = controller.search();
        if (!searchResult) {
            System.out.println("You tried to find something, but room is empty :(");
        }else {
            System.out.println("You found " + controller.getItemName());
            changeItemMenu();
        }
    }

    private void changeItemMenu(){
        int option;
        do {
            System.out.println();
            System.out.println("|***YOU HAVE FOUND SOMETHING***|");
            showChangeItemMenu();
            option = Integer.parseInt(scanner.nextLine());
            if (option == 1) {
                controller.changeItem();
                return;
            } else if (option == 2) {
                return;
            }
        } while (true);
    }

    private void showMenu() {
        System.out.println("Choose what to do: ");
        for (String option : menu) {
            System.out.println(option);
        }
        System.out.print(">>> ");
    }

    private void printFinalScene(){
        System.out.println("You walk around "
                + "the rooms but looks like they are "
                + "all empty and no enemies left");
        System.out.println("And then you finally found "
                + " a big room with cute sleeping dragon in it");
        System.out.println("The dragon awoke suddenly"
                + " and upon seeing you,"
                + " offered to become your companion");
        Design.printFinal();

    }
}
