package com.epam.training.view;

public interface Printable {
    void print();
}
