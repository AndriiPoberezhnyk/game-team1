package com.epam.training.view;

import com.epam.training.controller.PlayerController;
import com.epam.training.model.enemy.Monster;
import com.epam.training.model.player.Player;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

public class Design {
    private String[][] monster = new String[6][21];
    private String[] hero = new String[21];
    private String[] armoredHero = new String[21];
    private String[] chest = new String[8];
    private Design(){
        monster[0][0]="";
        monster[0][1]="             \\                  /";
        monster[0][2]="    _________))                ((__________";
        monster[0][3]="   /.-------./\\\\    \\    /    //\\.--------.\\";
        monster[0][4]="  //#######//##\\\\   ))  ((   //##\\\\########\\\\";
        monster[0][5]=" //#######//###((  ((    ))  ))###\\\\########\\\\";
        monster[0][6]="((#######((#####\\\\  \\\\  //  //#####))########))";
        monster[0][7]=" \\##' `###\\######\\\\  \\)(/  //######/####' `##/";
        monster[0][8]="  )'    ``#)'  `##\\`->oo<-'/##'  `(#''     `(";
        monster[0][9]="          (       ``\\`..'/''       )";
        monster[0][10]="                     \\\"\"(";
        monster[0][11]="                      `- )";
        monster[0][12]="                      / /";
        monster[0][13]="                     ( /\\";
        monster[0][14]="                     /\\| \\";
        monster[0][15]="                    (  \\";
        monster[0][16]="                        )";
        monster[0][17]="                       /";
        monster[0][18]="                      (";
        monster[0][19]="                      `";
        monster[0][20]="";
        monster[1][0]="";
        monster[1][1]="                   (    )";
        monster[1][2]="                  ((((()))";
        monster[1][3]="                  |o\\ /o)|";
        monster[1][4]="                  ( (  _')";
        monster[1][5]="                   (._.  /\\__";
        monster[1][6]="                  ,\\___,/ '  ')";
        monster[1][7]="    '.,_,,       (  .- .   .    )";
        monster[1][8]="     \\   \\\\     ( '        )(    )";
        monster[1][9]="      \\   \\\\    \\.  _.__ ____( .  |";
        monster[1][10]="       \\  /\\\\   .(   .'  /\\  '.  )";
        monster[1][11]="        \\(  \\\\.-' ( /    \\/    \\)";
        monster[1][12]="         '  ()) _'.-|/\\/\\/\\/\\/\\|";
        monster[1][13]="             '\\\\ .( |\\/\\/\\/\\/\\/|";
        monster[1][14]="               '((  \\    /\\    /";
        monster[1][15]="               ((((  '.__\\/__.')";
        monster[1][16]="                ((,) /   ((((  )";
        monster[1][17]="                 \"..-,   (()/ /";
        monster[1][18]="                  _//.     _/,/'";
        monster[1][19]="                 //,/\"    /,/,\"";
        monster[1][20]="";
        monster[2][0]="";
        monster[2][1]="";
        monster[2][2]="                '-.,;;:;,";
        monster[2][3]="                 _;\\;|\\;:;,";
        monster[2][4]="                ) __ ' \\;::,";
        monster[2][5]="            .--'  e   ':;;;:,           ;,";
        monster[2][6]="           (^           ;;::;          ;;;,";
        monster[2][7]="   _        --_.--.___,',:;::;     ,,,;:;;;,";
        monster[2][8]="  < \\        `;     |  ;:;;:;        ':;:;;;,,";
        monster[2][9]="<`-; \\__     ,;    /    ';:;;:,       ';;;'";
        monster[2][10]="<`_   __',   ; ,  /    ::;;;:         //";
        monster[2][11]="   `)|  \\ \\   ` .'      ';;:;,       //";
        monster[2][12]="    `    \\ `\\  /        ;;:;;.      //__";
        monster[2][13]="          \\  `/`         ;:;  ~._,=~`   `~=,";
        monster[2][14]="           \\_|      (        ^     ^  ^ _^  \\";
        monster[2][15]="             \\    _,`      / ^ ^  ^   .' `.^ ;";
        monster[2][16]="    <`-.      '-;`       /`  ^   ^  /\\    ) ^/";
        monster[2][17]="    <'- \\__..-'` ___,,,-'._ ^  ^ _.'\\^`'-' ^/";
        monster[2][18]="     `)_   ..-''`          `~~~~`    `~===~`";
        monster[2][19]="     <_.-`-._\\ ";
        monster[2][20]="";
        monster[3][0]="  \\";
        monster[3][1]="   \\\\       __";
        monster[3][2]="    \\\\ /=/ =/-";
        monster[3][3]="  --=\\\\---=/_=--";
        monster[3][4]="  --\\/\\\\ \\/  \\==_-";
        monster[3][5]="--==/O   O\\   \\==";
        monster[3][6]="  --\\  [  /    \\_\\     _ _ _ _";
        monster[3][7]="  -==[ [ [       \\    / -) ) /\\";
        monster[3][8]="   --(ˎ_ˎ)  /     \\__/-/_/_/_ /)";
        monster[3][9]="    -/     /                 \\_/)";
        monster[3][10]="    (\\ ]  / )   )       \\      \\/";
        monster[3][11]="    ( \\./ _/   /         )      /";
        monster[3][12]="    /  (      /         /       )";
        monster[3][13]="   /  _/\\ \\---/ _ _ _ _\\/       )";
        monster[3][14]="  /  /   \\ \\____ \\     / \\     /";
        monster[3][15]="  (  )   ( __\\  \\ \\   (   \\   )";
        monster[3][16]="   \\ \\       `---' \\__ \\   (  )";
        monster[3][17]="    \\ \\_             / /   \\  /";
        monster[3][18]="     \\/__\\         _/_(     ] ]";
        monster[3][19]="                  /___]     /_\\";
        monster[3][20]="                           (___)";
        monster[4][0]="         =*===";
        monster[4][1]="       $$> < $$$";
        monster[4][2]="       $ <   D$$";
        monster[4][3]="       $ __ $$$";
        monster[4][4]=" ,     $$$$  |";
        monster[4][5]="///; ,---' _ |----.";
        monster[4][6]=" \\ )(           /  )";
        monster[4][7]=" | \\/ \\.   '  _.|  \\              $";
        monster[4][8]=" |  \\ /(   /    /\\_ \\          $$$$$";
        monster[4][9]="  \\ /  (       / /  )         $$$ $$$";
        monster[4][10]="       (  ,   /_/ ,`_,-----.,$$  $$$";
        monster[4][11]="       |   <----|  \\---##     \\   $$";
        monster[4][12]="       /         \\\\\\           |    $";
        monster[4][13]="      |                 \\      /";
        monster[4][14]="      /  \\_|    /______,/     /";
        monster[4][15]="     /   / |   /    |   |    /";
        monster[4][16]="    (   /--|  /.     \\  (\\  (_";
        monster[4][17]="     `----,( ( _\\     \\ / / ,/";
        monster[4][18]="           | /        /,_/,/";
        monster[4][19]="          _|/        / / (";
        monster[4][20]="        /, |        ^-/, |";
        monster[5][0]="";
        monster[5][1]="";
        monster[5][2]="";
        monster[5][3]="                     ___====-_  _-====___";
        monster[5][4]="              __--^^^      //     \\\\     ^^^--_";
        monster[5][5]="             _-^         // (    ) \\\\         ^-_";
        monster[5][6]="            -           //  |\\^^/|  \\\\           -";
        monster[5][7]="          _/           //   (0::0)   \\\\            \\_";
        monster[5][8]="         /            ((     \\\\//     ))             \\";
        monster[5][9]="       -               \\\\    (oo)    //               -";
        monster[5][10]="      -                 \\\\  / \\/ \\  //                 -";
        monster[5][11]="     -                   \\\\/      \\//                   -";
        monster[5][12]="   / /|           /\\      (        )      /\\           |\\ \\";
        monster[5][13]="   |/ | /\\_/\\_/\\_/  \\_/\\  (   /\\   )  /\\_/  \\_/\\_/\\_/\\ | \\|";
        monster[5][14]="   `  |/  V  V  `    V  \\_(| |  | |)_/  V    '  V  V  \\|  '";
        monster[5][15]="      `   `  `       `   / | |  | | \\   '       '  '   '";
        monster[5][16]="                       <(  | |  | |  )>";
        monster[5][17]="                      <__\\_| |  | |_\\__>";
        monster[5][18]="                      ^^^^ ^^^  ^^^ ^^^^^";
        monster[5][19]="";
        monster[5][20]="";
        chest[0]="\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t __________";
        chest[1]="\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t/___;;____/\\";
        chest[2]="\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\\         \\ |";
        chest[3]="\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t  . ()oo(() .`";
        chest[4]="\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t/^()^^*()%)/|";
        chest[5]="\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t|-------%-| |%";
        chest[6]="\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t|   ((  % | \\ %";
        chest[7]="\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t|________%|\\  %";
        hero[0]="\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t";
        hero[1]="\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t";
        hero[2]="\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t";
        hero[3]="\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t";
        hero[4]="\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t";
        hero[5]="\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t";
        hero[6]="\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t";
        hero[7]="\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t";
        hero[8]="\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t";
        hero[9]="\t\t\t\t\t\t  ()\t\t\t\t\t\t\t\t\t\t\t\t\t";
        hero[10]="\t\t\t\t\t\t  ||    __\t\t\t\t\t\t\t\t\t\t\t\t";
        hero[11]="\t\t\t\t\t\t  | \\_,(**),__ \t\t\t\t\t\t\t\t\t\t\t";
        hero[12]="\t\t\t\t\t\t  `\\ ' `--'  ),\t\t\t\t\t\t\t\t\t\t\t";
        hero[13]="\t\t\t\t\t\t    `\\_. ._/\\\\\t\t\t\t\t\t\t\t\t\t\t";
        hero[14]="\t\t\t\t\t\t      | . |  \\\\\t\t\t\t\t\t\t\t\t\t\t";
        hero[15]="\t\t\t\t\t\t     /==0==\\  ()\t\t\t\t\t\t\t\t\t\t";
        hero[16]="\t\t\t\t\t\t    /~/   \\~\\\t\t\t\t\t\t\t\t\t\t\t";
        hero[17]="\t\t\t\t\t\t   /~/     \\~\\\t\t\t\t\t\t\t\t\t\t\t";
        hero[18]="\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t";
        hero[19]="\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t";
        hero[20]="\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t";
        armoredHero[0]="\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t";
        armoredHero[1]="\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t";
        armoredHero[2]="\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t";
        armoredHero[3]="\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t";
        armoredHero[4]="\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t";
        armoredHero[5]="\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t";
        armoredHero[6]="\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t";
        armoredHero[7]="\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t";
        armoredHero[8]="\t\t\t\t\t\t   _\t\t\t\t\t\t\t\t\t\t\t\t\t";
        armoredHero[9]="\t\t\t\t\t\t==(W{==========-\t\t\t\t\t\t\t\t\t\t";
        armoredHero[10]="\t\t\t\t\t\t  ||  (.--.)\t\t\t\t\t\t\t\t\t\t\t";
        armoredHero[11]="\t\t\t\t\t\t  | \\_,|**|,__ \t\t\t\t\t\t\t\t\t\t\t";
        armoredHero[12]="\t\t\t\t\t\t  `\\ ' `--'   ),\t\t\t\t\t\t\t\t\t\t";
        armoredHero[13]="\t\t\t\t\t\t   /`\\_. .__/\\\\\t\t\t\t\t\t\t\t\t\t\t";
        armoredHero[14]="\t\t\t\t\t\t  (   | .  |  \\\\\t\t\t\t\t\t\t\t\t\t";
        armoredHero[15]="\t\t\t\t\t\t  )__/==0==\\   ()\t\t\t\t\t\t\t\t\t\t";
        armoredHero[16]="\t\t\t\t\t\t    /~\\___/~\\\t\t\t\t\t\t\t\t\t\t\t";
        armoredHero[17]="\t\t\t\t\t\t   /~/     \\~\\\t\t\t\t\t\t\t\t\t\t\t";
        armoredHero[18]="\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t";
        armoredHero[19]="\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t";
        armoredHero[20]="\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t";
    }

    public static void heroPrint(PlayerController controller) {
        Design base = new Design();
        int level = controller.getCurrentLocation();

        if (!controller.hasMonster()){
            if(!controller.isChecked()) {
                System.out.println(base.chest[0]);
                System.out.println(base.chest[1]);
                System.out.println(base.chest[2]);
                System.out.println(base.chest[3]);
                System.out.println(base.chest[4]);
                System.out.println(base.chest[5]);
                System.out.println(base.chest[6]);
                System.out.println(base.chest[7]);
            }else {
                System.out.print("\n\n\n\n\n\n\n\n\n");
            }
        } else {
            System.out.println(base.hero[0]+base.monster[level][0]);
            System.out.println(base.hero[1]+base.monster[level][1]);
            System.out.println(base.hero[2]+base.monster[level][2]);
            System.out.println(base.hero[3]+base.monster[level][3]);
            System.out.println(base.hero[4]+base.monster[level][4]);
            System.out.println(base.hero[5]+base.monster[level][5]);
            System.out.println(base.hero[6]+base.monster[level][6]);
            System.out.println(base.hero[7]+base.monster[level][7]);
        }
        if (controller.getArmorSet().getItem("Weapon").getBuffPoints()>0) {
            if (!controller.hasMonster()){
                System.out.println(base.armoredHero[8]);
                System.out.println(base.armoredHero[9]);
            } else {
                System.out.println(base.armoredHero[8]+base.monster[level][8]);
                System.out.println(base.armoredHero[9]+base.monster[level][9]);
            }
        } else {
            if (!controller.hasMonster()){
                System.out.println(base.hero[8]);
                System.out.println(base.hero[9]);
            } else {
                System.out.println(base.hero[8]+base.monster[level][8]);
                System.out.println(base.hero[9]+base.monster[level][9]);
            }
        }
        if (controller.getArmorSet().getItem("Helmet").getBuffPoints()>0) {
            if (!controller.hasMonster()){
                System.out.println(base.armoredHero[10]);
                System.out.println(base.armoredHero[11]);
            } else {
                System.out.println(base.armoredHero[10]+base.monster[level][10]);
                System.out.println(base.armoredHero[11]+base.monster[level][11]);
            }
        } else {
            if (!controller.hasMonster()){
                System.out.println(base.hero[10]);
                System.out.println(base.hero[11]);
            } else {
                System.out.println(base.hero[10]+base.monster[level][10]);
                System.out.println(base.hero[11]+base.monster[level][11]);
            }

        }
        if (controller.getArmorSet().getItem("Armor").getBuffPoints()>0) {
            if (!controller.hasMonster()){
                System.out.println(base.armoredHero[12]);
                System.out.println(base.armoredHero[13]);
                System.out.println(base.armoredHero[14]);
                System.out.println(base.armoredHero[15]);
            } else {
                System.out.println(base.armoredHero[12]+base.monster[level][12]);
                System.out.println(base.armoredHero[13]+base.monster[level][13]);
                System.out.println(base.armoredHero[14]+base.monster[level][14]);
                System.out.println(base.armoredHero[15]+base.monster[level][15]);
            }
        } else {
            if (!controller.hasMonster()){
                System.out.println(base.hero[12]);
                System.out.println(base.hero[13]);
                System.out.println(base.hero[14]);
                System.out.println(base.hero[15]);
            } else {
                System.out.println(base.hero[12]+base.monster[level][12]);
                System.out.println(base.hero[13]+base.monster[level][13]);
                System.out.println(base.hero[14]+base.monster[level][14]);
                System.out.println(base.hero[15]+base.monster[level][15]);
            }
        }
        if (controller.getArmorSet().getItem("Trousers").getBuffPoints()>0) {
            if (!controller.hasMonster()){
                System.out.println(base.armoredHero[16]);
            } else {
                System.out.println(base.armoredHero[16]+base.monster[level][16]);
            }
        } else {
            if (!controller.hasMonster()){
                System.out.println(base.hero[16]);
            } else {
                System.out.println(base.hero[16]+base.monster[level][16]);
            }
        }
        if (controller.getArmorSet().getItem("Boots").getBuffPoints()>0) {
            if (!controller.hasMonster()){
                System.out.println(base.armoredHero[17]);
            } else {
                System.out.println(base.armoredHero[17]+base.monster[level][17]);
            }
        } else {
            if (!controller.hasMonster()){
                System.out.println(base.hero[17]);
            } else {
                System.out.println(base.hero[17]+base.monster[level][17]);
            }
        }
        if (!controller.hasMonster()){
            System.out.println("\n\n\n");
        } else {
            System.out.println(base.hero[17]+base.monster[level][17]);
            System.out.println(base.hero[18]+base.monster[level][18]);
            System.out.println(base.hero[19]+base.monster[level][19]);
            System.out.println(base.hero[20]+base.monster[level][20]);
        }

    }

    public static void printCastle(){
        StringBuilder sb = new StringBuilder();
        try (FileReader reader = new FileReader("GUI.txt");
            BufferedReader br = new BufferedReader(reader)) {
            sb.append(br.readLine());
            while(!sb.toString().trim().equals("Final Scene")) {
                System.out.println(sb);
                sb = new StringBuilder();
                sb.append(br.readLine());
            }
        } catch (IOException e) {
            System.err.format("IOException: %s%n", e);
        }
    }

    public static void printFinal(){
        StringBuilder sb = new StringBuilder();
        try (FileReader reader = new FileReader("GUI.txt");
             BufferedReader br = new BufferedReader(reader)) {
             sb.append(br.readLine());
             while(!sb.toString().trim().equals("Final Scene")) {
                  sb = new StringBuilder();
                  sb.append(br.readLine());
             }
             sb = new StringBuilder();
             sb.append(br.readLine());
             while(!sb.toString().trim().equals("Death final")){
                 System.out.println(sb);
                 sb = new StringBuilder();
                 sb.append(br.readLine());
             }
        } catch (IOException e) {
            System.err.format("IOException: %s%n", e);
        }
    }

    public static void printIfDead(){
        StringBuilder sb = new StringBuilder();
        try (FileReader reader = new FileReader("GUI.txt");
             BufferedReader br = new BufferedReader(reader)) {
            sb.append(br.readLine());
            while(!sb.toString().trim().equals("Death final")) {
                sb = new StringBuilder();
                sb.append(br.readLine());
            }
            sb = new StringBuilder();
            sb.append(br.readLine());
            while(!sb.toString().trim().equals("null")){
                System.out.println(sb);
                sb = new StringBuilder();
                sb.append(br.readLine());
            }
        } catch (IOException e) {
            System.err.format("IOException: %s%n", e);
        }
    }

    public static void printPlayerInfo(Player player){
        System.out.print(" HEALTH = "+ player.getHealth());
        System.out.print(" ARMOR = "+ player.getArmor());
        System.out.print(" DAMAGE = "+ player.getDamage());
        System.out.print(" SPEED = "+ player.getSpeed());
    }

    public static void printMonsterInfo(Monster monster){
        System.out.print(" HEALTH = "+ monster.getHealth());
        System.out.print(" ARMOR = "+ monster.getArmor());
        System.out.print(" DAMAGE = "+ monster.getDamage());
        System.out.print(" SPEED = "+ monster.getSpeed() + "\n");
    }

    public static void locationPrint(){
        System.out.print("_ ___ ___ ___ ___ ___ ___ ___ ___ ___ ___ ___ ___ ___ ___ ___ ___ ___ ___ ___ ___ ___ ___ ___ ___ ___ ___ ___ ___ ___ ___ ___ ___ ___ ___\n" +
                "_|___|___|___|___|___|___|___|___|___|___|___|___|___|___|___|___|___|___|___|___|___|___|___|___|___|___|___|___|___|___|___|___|___|___|\n" +
                "___|___|___|___|___|___|___|___|___|___|___|___|___|___|___|___|___|___|___|___|___|___|___|___|___|___|___|___|___|___|___|___|___|___|__\n" +
                "_|___|___|___|___|___|___|___|___|___|___|___|___|___|___|___|___|___|___|___|___|___|___|___|___|___|___|___|___|___|___|___|___|___|___|\n" +
                "___|___|___|___|___|___|___|___|___|___|___|___|___|___|___|___|___|___|___|___|___|___|___|___|___|___|___|___|___|___|___|___|___|___|__\n" +
                "_|___|___|___|___|___|___|___|__|     |__|___|___|___|___|___|___|___|___|___|___|___|___|___|___|__|     |__|___|___|___|___|___|___|___|\n" +
                "___|___|___|___|___|___|___|__|         |__|___|___|___|___|___|___|___|___|___|___|___|___|___|__|         |__|___|___|___|___|___|___|__\n" +
                "_|___|___|___|___|___|___|___|           |___|___|___|___|___|___|___|___|___|___|___|___|___|___|           |___|___|___|___|___|___|___|\n" +
                "___|___|___|___|___|___|___|_|           |_|___|___|___|___|___|___|___|___|___|___|___|___|___|_|           |_|___|___|___|___|___|___|__\n" +
                "_|___|___|___|___|___|___|___|           |___|___|___|___|___|___|___|___|___|___|___|___|___|___|           |___|___|___|___|___|___|___|\n" +
                "___|___|___|___|___|___|___|_|           |_|___|___|___|___|___|___|___|___|___|___|___|___|___|_|           |_|___|___|___|___|___|___|__\n" +
                "_|___|___|___|___|___|___|___|           |___|___|___|___|___|___|___|___|___|___|___|___|___|___|           |___|___|___|___|___|___|___|\n" +
                "___|___|___|___|___|___|___|_|           |_|___|___|___|___|___|___|___|___|___|___|___|___|___|_|           |_|___|___|___|___|___|___|__\n" +
                "_|___|___|___|___|___|___|___|_ ___ ___ _|___|___|___|___|___|___|___|___|___|___|___|___|___|___|_ ___ ___ _|___|___|___|___|___|___|___|\n" +
                "___|___|___|___|___|___|___|___|___|___|___|___|___|___|___|___|___|___|___|___|___|___|___|___|___|___|___|___|___|___|___|___|___|___|__\n" +
                "_|___|___|___|___|___|___|___|___|___|___|___|___|___|___|___|___|___|___|___|___|___|___|___|___|___|___|___|___|___|___|___|___|___|___|\n" +
                "___|___|___|___|___|___|___|___|___|___|___|___|___|___|___|___|___|___|___|___|___|___|___|___|___|___|___|___|___|___|___|___|___|___|__\n" +
                "_|___|___|___|___|___|___|___|___|___|___|___|___|___|___|___|___|___|___|___|___|___|___|___|___|___|___|___|___|___|___|___|___|___|___|\n" +
                "___|___|___|___|___|___|___|___|___|___|___|___|___|___|___|___|___|___|___|___|___|___|___|___|___|___|___|___|___|___|___|___|___|___|__\n");
    }

    public static String[] readCharacter(String a, String b){
        String [] character = new String[21];
        int i=0;
        StringBuilder sb = new StringBuilder();
        try (FileReader reader = new FileReader("characters.txt");
             BufferedReader br = new BufferedReader(reader)) {
            sb.append(br.readLine());
            while(!sb.toString().trim().equals(a)) {
                sb = new StringBuilder();
                sb.append(br.readLine());
            }
            sb = new StringBuilder();
            sb.append(br.readLine());
            while(!sb.toString().trim().equals(b)){
                character[i]=sb.toString();
                i++;
                sb = new StringBuilder();
                sb.append(br.readLine());
            }
        } catch (IOException e) {
            System.err.format("IOException: %s%n", e);
        }
        return character;
    }

}
