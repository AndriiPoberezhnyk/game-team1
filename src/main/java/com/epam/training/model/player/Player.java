package com.epam.training.model.player;

import com.epam.training.model.item.ArmorSet;
import com.epam.training.model.item.Item;

public class Player {
    public static final int BASE_HEALTH = 150;
    private static final int BASE_ARMOR = 0;
    private static final int BASE_SPEED = 3;
    private static final int BASE_DAMAGE = 50;
    private static final ArmorSet ARMOR_SET = new ArmorSet();
    private int health;
    private int armor;
    private int speed;
    private int damage;
    private ArmorSet armorSet;
    private boolean isInBattle;

    public Player() {
        health = BASE_HEALTH;
        armor = BASE_ARMOR;
        speed = BASE_SPEED;
        damage = BASE_DAMAGE;
        armorSet = ARMOR_SET;
        isInBattle = false;
    }

    public boolean isInBattle() {
        return isInBattle;
    }

    public void setInBattle(boolean inBattle) {
        isInBattle = inBattle;
    }

    public void takeDamage(int damage) {
        health -= damage - damage * armor / 100;
    }

    public boolean isLive() {
        return health > 0;
    }

    public void useItem(Item item) {
        if (item.getName().equals("Drug")) {
            health += item.getBuffPoints();
        } else {
            Item currentItem = armorSet.getItem(item.getName());
            if (item.getName().equals("Weapon")) {
                if (currentItem != null)
                    damage -= armorSet.getItem(item.getName()).getBuffPoints();
                armorSet.setItem(item);
                damage += armorSet.getItem(item.getName()).getBuffPoints();
            }else if (item.getName().equals("Boots")) {
                if (currentItem != null)
                     speed -= armorSet.getItem(item.getName()).getBuffPoints();
                armorSet.setItem(item);
                speed += armorSet.getItem(item.getName()).getBuffPoints();
            }else {
                if (currentItem != null)
                     armor -= armorSet.getItem(item.getName()).getBuffPoints();
                armorSet.setItem(item);
                armor += armorSet.getItem(item.getName()).getBuffPoints();
            }
        }
    }

    public int getHealth() {
        return health;
    }

    public void setHealth(int health) {
        this.health = health;
    }

    public int getArmor() {
        return armor;
    }

    public void setArmor(int armor) {
        this.armor = armor;
    }

    public int getSpeed() {
        return speed;
    }

    public void setSpeed(int speed) {
        this.speed = speed;
    }

    public int getDamage() {
        return damage;
    }

    public void setDamage(int damage) {
        this.damage = damage;
    }

    public ArmorSet getArmorSet() {
        return armorSet;
    }

    public void setArmorSet(ArmorSet armorSet) {
        this.armorSet = armorSet;
    }

    @Override
    public String toString() {
        return "Player{" +
                "health=" + health +
                ", armor=" + armor +
                ", damage=" + damage +
                ", speed=" + speed +
                '}';
    }
}
