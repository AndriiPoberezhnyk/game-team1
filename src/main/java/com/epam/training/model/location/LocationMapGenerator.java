package com.epam.training.model.location;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;

public class LocationMapGenerator implements LocationListModel {
    public static final int FLOORS = 3;
    public static final int ROOMS = 6;
    private Map<Integer, LinkedList<LocationGenerator>> locations;

    public LocationMapGenerator() {
        locations = new HashMap<>();
        for (int i = 0; i < FLOORS; i++) {
            locations.put(i, generateFloor(i));
        }
    }

    public Map<Integer, LinkedList<LocationGenerator>> getLocationList() {
        return locations;
    }

    private LinkedList<LocationGenerator> generateFloor(int floor) {
        LinkedList<LocationGenerator> generatedFloor = new LinkedList<>();
        for (int i = 0; i < ROOMS; i++) {
            generatedFloor.add(new LocationGenerator(floor, i));
        }
        generatedFloor.peekFirst().getLocation().setHasStairs(true);
        generatedFloor.peekLast().getLocation().setHasStairs(true);
        return generatedFloor;
    }
}
