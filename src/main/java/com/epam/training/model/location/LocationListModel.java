package com.epam.training.model.location;

import java.util.LinkedList;
import java.util.Map;

public interface LocationListModel {
    Map<Integer, LinkedList<LocationGenerator>> getLocationList();
}
