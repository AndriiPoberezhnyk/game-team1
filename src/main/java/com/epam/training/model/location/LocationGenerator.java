package com.epam.training.model.location;

import com.epam.training.model.enemy.Monster;
import com.epam.training.model.item.Item;

public class LocationGenerator implements LocationModel {
    private Location location;

    public LocationGenerator(int floor, int room) {
        this.location = new Location(floor, room);
    }
    @Override
    public Location getLocation() {
        return location;
    }

    @Override
    public Item getItem() {
        return location.getItem();
    }

    @Override
    public Monster getMonster() {
        return location.getMonster();
    }

    @Override
    public boolean isChecked() {
        return location.isChecked();
    }

    @Override
    public boolean hasStairs() {
        return location.hasStairs();
    }

    @Override
    public void setChecked(boolean checked) {
        location.setChecked(checked);
    }
}
