package com.epam.training.model.location;

import com.epam.training.model.enemy.Monster;
import com.epam.training.model.item.Item;

public interface LocationModel {
    Location getLocation();
    Item getItem();
    Monster getMonster();
    boolean isChecked();
    boolean hasStairs();
    void setChecked(boolean checked);
}
