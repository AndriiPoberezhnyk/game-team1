package com.epam.training.model.location;

import com.epam.training.model.enemy.Monster;
import com.epam.training.model.item.*;

import java.util.Random;

public class Location {
    private static final int ITEMS = 6;
    private Item item;
    private Monster monster;
    private boolean hasStairs;
    private boolean isChecked;

    public Location(int floor, int room) {
        floor++;
        generateItem(floor,room);
        generateMonster(floor,room);
        hasStairs = false;
        isChecked = false;
    }

    private void generateItem(int floor, int room) {
        Random random = new Random();
        if (random.nextBoolean()) {
            int itemType = 1 + random.nextInt(ITEMS);
            switch (itemType) {
                case 1:
                    item = new Weapon(floor, room);
                    break;
                case 2:
                    item = new Armor(floor, room);
                    break;
                case 3:
                    item = new Boots(floor, room);
                    break;
                case 4:
                    item = new Helmet(floor, room);
                    break;
                case 5:
                    item = new Trousers(floor, room);
                    break;
                case 6:
                    item = new Drug(floor, room);
                    break;
            }
        } else {
            item = null;
        }
    }

    private void generateMonster(int floor, int room){
        Random random = new Random();
        if (random.nextBoolean()) {
            monster = new Monster(floor, room);
        }else {
            monster = null;
        }
    }

    public Item getItem() {
        return item;
    }

    public Monster getMonster() {
        return monster;
    }

    public boolean hasStairs() {
        return hasStairs;
    }

    public boolean isChecked() {
        return isChecked;
    }

    public void setChecked(boolean checked) {
        isChecked = checked;
    }

    public void setHasStairs(boolean hasStairs) {
        this.hasStairs = hasStairs;
    }


}
