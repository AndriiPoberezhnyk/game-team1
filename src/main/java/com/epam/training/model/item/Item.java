package com.epam.training.model.item;

public abstract class Item {
    private String name;
    private int buffPoints;

    public Item(String name, int buffPoints, int floor, int room) {
        this.name = name;
        this.buffPoints = generateBuffPoints(buffPoints, floor, room);
    }

    private int generateBuffPoints(int buffPoints, int floor, int room){
        return (int)(buffPoints*floor + buffPoints*0.1*room);
    }

    public int getBuffPoints(){
        return buffPoints;
    }

    public String getName() {
        return name;
    }

    @Override
    public String toString() {
        return name + " " + buffPoints;
    }
}
