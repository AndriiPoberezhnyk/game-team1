package com.epam.training.model.item;

public class Weapon extends Item {
    private static final int BASE_VALUE = 50;

    public Weapon(int floor, int room) {
        super("Weapon", BASE_VALUE, floor, room);
    }
}
