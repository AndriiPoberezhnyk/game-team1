package com.epam.training.model.item;

public class Helmet extends Item {
    private static final int BASE_VALUE = 5;
    public Helmet(int floor, int room) {
        super("Helmet", BASE_VALUE, floor, room);
    }
}
