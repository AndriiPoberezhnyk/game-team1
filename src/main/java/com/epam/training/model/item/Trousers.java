package com.epam.training.model.item;

public class Trousers extends Item {
    private static final int BASE_VALUE = 10;

    public Trousers(int floor, int room) {
        super("Trousers", BASE_VALUE, floor, room);
    }
}
