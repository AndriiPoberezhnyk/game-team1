package com.epam.training.model.item;

import java.util.LinkedHashMap;
import java.util.Map;

public class ArmorSet {
    private Map<String, Item> setters;

    public ArmorSet() {
        setters = new LinkedHashMap<>();
        setters.put("Armor", new Armor(0, 0));
        setters.put("Trousers", new Trousers(0,0));
        setters.put("Boots", new Boots(0,0));
        setters.put("Helmet", new Helmet(0,0));
        setters.put("Weapon", new Weapon(0,0));
    }

    public void setItem(Item item){
        for(String itemKey: setters.keySet()){
            if(itemKey.equals(item.getName())){
                setters.put(itemKey, item);
            }
        }
    }

    public Item getItem(String itemKey){
        return setters.get(itemKey);
    }
}
