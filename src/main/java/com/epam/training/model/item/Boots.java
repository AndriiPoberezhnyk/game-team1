package com.epam.training.model.item;

public class Boots extends Item {
    private static final int BASE_VALUE = 10;

    public Boots(int floor, int room) {
        super("Boots", BASE_VALUE, floor, room);
    }
}
