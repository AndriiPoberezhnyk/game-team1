package com.epam.training.model.item;

public class Armor extends Item {
    private static final int BASE_VALUE = 20;

    public Armor(int floor, int room) {
        super("Armor", BASE_VALUE, floor, room);
    }
}
