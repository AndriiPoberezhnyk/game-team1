package com.epam.training.model.item;

public class Drug extends Item {
    private static final int BASE_VALUE = 50;

    public Drug(int floor, int room) {
        super("Drug", BASE_VALUE, floor, room);
    }
}
