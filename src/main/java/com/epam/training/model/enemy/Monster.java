package com.epam.training.model.enemy;

public class Monster {
    private static final int BASE_HEALTH = 150;
    private static final int BASE_ARMOR = 10;
    private static final int BASE_DAMAGE = 15;
    private static final int BASE_SPEED = 5;

    private int health;
    private int armor;
    private int damage;
    private int speed;

    public Monster(int floor, int room) {
        this.health = generateStat(BASE_HEALTH, floor, room);
        this.armor = generateStat(BASE_ARMOR, floor, room);
        this.damage = generateStat(BASE_DAMAGE, floor, room);
        this.speed = generateStat(BASE_SPEED, floor, room);
    }

    private int generateStat(int basePoints, int floor, int room){
        return (int)(basePoints*floor + basePoints*0.1*room);
    }

    public int getHealth() {
        return health;
    }

    public void setHealth(int health) {
        this.health = health;
    }

    public int getArmor() {
        return armor;
    }

    public void setArmor(int armor) {
        this.armor = armor;
    }

    public int getDamage() {
        return damage;
    }

    public void setDamage(int damage) {
        this.damage = damage;
    }

    public int getSpeed() {
        return speed;
    }

    public void setSpeed(int speed) {
        this.speed = speed;
    }

    @Override
    public String toString() {
        return "Monster{" +
                "health=" + health +
                ", armor=" + armor +
                ", damage=" + damage +
                ", speed=" + speed +
                '}';
    }

    public void takeDamage(int damage){
        health -= damage - damage * armor / 100;
    }

    public boolean isLive(){
        return health > 0;
    }
}
