package com.epam.training.controller;

import com.epam.training.model.enemy.Monster;
import com.epam.training.model.item.Item;
import com.epam.training.model.location.LocationModel;

public interface LocationController {
    Item getItem();
    int getCurrentLocationIndex();
    int getCurrentFloor();
    int getLastFloorIndex();
    int getLastLocationIndex();
    String getLocationInfo();
    Monster getMonster();
    void killMonster();
    void moveNextLocation();
    void movePreviousLocation();
    void moveUpstairs();
    void moveDownstairs();
    boolean hasUpperFloor();
    boolean hasLowerFloor();
    boolean hasNextLocation();
    boolean hasPreviousLocation();
    boolean isChecked();
    boolean hasMonster();
    boolean hasStairs();
    LocationModel getLocation();
    boolean canMoveUp();
    boolean isAnyMonsters();
}
