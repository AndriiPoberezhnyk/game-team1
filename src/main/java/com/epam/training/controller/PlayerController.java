package com.epam.training.controller;

import com.epam.training.model.enemy.Monster;
import com.epam.training.model.item.ArmorSet;
import com.epam.training.model.item.Item;
import com.epam.training.model.player.Player;

import java.util.Random;

public class PlayerController implements PlayerControllerInterface {
    private static final int HP_REGEN = 10;
    private Player player;
    private LocationController locationController;
    private Item foundItem;

    public PlayerController() {
        player = new Player();
        locationController = new LocControllerImpl();
        foundItem = null;
    }

    @Override
    public boolean hasUpperFloor() {
        return locationController.hasUpperFloor();
    }

    @Override
    public boolean hasLowerFloor() {
        return locationController.hasLowerFloor();
    }

    @Override
    public void runForward() {
        if (getMonster().getSpeed() > player.getSpeed()) {
            player.takeDamage(getMonster().getDamage());
            goToNextLocation();
        } else {
            goToNextLocation();
        }
    }

    @Override
    public void runBack() {
        if (getMonster().getSpeed() > player.getSpeed()) {
            player.takeDamage(getMonster().getDamage());
            goToPreviousLocation();
        } else {
            goToPreviousLocation();
        }
    }

    @Override
    public void runDownstairs() {
        if (getMonster().getSpeed() > player.getSpeed()) {
            player.takeDamage(getMonster().getDamage());
            goToPreviousFloor();
        } else {
            goToPreviousFloor();
        }
    }

    @Override
    public void attack() {
        Random random = new Random();
        int successfulHit = random.nextInt(100) + 1;
        if (successfulHit > 15) {
            getMonster().takeDamage(player.getDamage());
        } else {
            System.out.println("You missed");
        }
        successfulHit = random.nextInt(100) + 1;
        if (getMonster().isLive()) {
            if (successfulHit > 20) {
                player.takeDamage(getMonster().getDamage());
            }else{
                System.out.println("Wow you dodged the attack");
            }
        } else {
            locationController.killMonster();
            System.out.println("Monster defeated");
        }
    }

    @Override
    public boolean isPlayerLive() {
        return player.isLive();
    }

    @Override
    public boolean search() {
        foundItem = locationController.getItem();
        return foundItem != null;
    }

    @Override
    public void goToNextLocation() {
        int currentHealth = player.getHealth();
        if (currentHealth > (player.BASE_HEALTH - 10)) {
            if (currentHealth < player.BASE_HEALTH)
                player.setHealth(player.BASE_HEALTH);
        } else {
            player.setHealth(currentHealth + HP_REGEN);
        }
        locationController.moveNextLocation();
    }

    @Override
    public void goToPreviousLocation() {
        int currentHealth = player.getHealth();
        if (currentHealth > (player.BASE_HEALTH - 10)) {
            if (currentHealth < player.BASE_HEALTH)
                player.setHealth(player.BASE_HEALTH);
        } else {
            player.setHealth(currentHealth + HP_REGEN);
        }
        locationController.movePreviousLocation();
    }

    @Override
    public void goToNextFloor() {
        locationController.moveUpstairs();
    }

    @Override
    public void goToPreviousFloor() {
        locationController.moveDownstairs();
    }

    @Override
    public void changeItem() {
        player.useItem(foundItem);
    }

    @Override
    public String getItemName() {
        return foundItem.toString();
    }

    @Override
    public String getPlayerItemName() {
        return player.getArmorSet().getItem(foundItem.getName()).toString();
    }

    @Override
    public boolean isChecked() {
        return locationController.isChecked();
    }

    @Override
    public Monster getMonster() {
        return locationController.getMonster();
    }

    @Override
    public boolean hasMonster() {
        return locationController.hasMonster();
    }

    @Override
    public boolean hasStairs() {
        return locationController.hasStairs();
    }

    @Override
    public boolean hasNextLocation() {
        return locationController.hasNextLocation();
    }

    @Override
    public boolean hasPreviousLocation() {
        return locationController.hasPreviousLocation();
    }

    @Override
    public boolean isInBattle() {
        return player.isInBattle();
    }

    public int getCurrentLocation() {
        return locationController.getCurrentLocationIndex();
    }

    public int getCurrentFloor() {
        return locationController.getCurrentFloor();
    }

    @Override
    public boolean canMoveUp() {
        return locationController.canMoveUp();
    }

    @Override
    public ArmorSet getArmorSet() {
        return player.getArmorSet();
    }

    @Override
    public boolean isLocationsEmpty() {
        return locationController.isAnyMonsters();
    }

    @Override
    public Player getPlayer() {
        return player;
    }
}
