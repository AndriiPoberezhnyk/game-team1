package com.epam.training.controller;

import com.epam.training.model.enemy.Monster;
import com.epam.training.model.item.ArmorSet;
import com.epam.training.model.player.Player;

public interface PlayerControllerInterface {

    String getItemName();
    String getPlayerItemName();
    void runForward();
    void runBack();
    void runDownstairs();
    void changeItem();
    void goToNextLocation();
    void goToPreviousLocation();
    void goToNextFloor();
    void goToPreviousFloor();
    boolean search();
    void attack();
    boolean isPlayerLive();
    boolean isInBattle();
    boolean isChecked();
    boolean hasMonster();
    boolean hasUpperFloor();
    boolean hasLowerFloor();
    boolean hasStairs();
    boolean hasNextLocation();
    boolean hasPreviousLocation();
    boolean canMoveUp();
    ArmorSet getArmorSet();
    boolean isLocationsEmpty();
    Player getPlayer();
    Monster getMonster();
}
