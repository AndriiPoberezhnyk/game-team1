package com.epam.training.controller;

import com.epam.training.model.enemy.Monster;
import com.epam.training.model.item.Item;
import com.epam.training.model.location.LocationGenerator;
import com.epam.training.model.location.LocationMapGenerator;
import com.epam.training.model.location.LocationListModel;
import com.epam.training.model.location.LocationModel;

import java.util.LinkedList;

public class LocControllerImpl implements LocationController {
    private static final int FIRST_FLOOR = 0;
    private static final int FIRST_LOCATION = 0;
    private int[] monstersOnFloor;
    private int currentLocationIndex;
    private int currentFloor;
    private LocationModel currentLocation;
    private LocationListModel locGenerator;

    public LocControllerImpl() {
        locGenerator = new LocationMapGenerator();
        currentFloor = FIRST_FLOOR;
        currentLocationIndex = FIRST_LOCATION;
        currentLocation = getLocation();
        fillMonstersArray();
    }

    private void fillMonstersArray() {
        monstersOnFloor = new int[LocationMapGenerator.FLOORS];
        for (int i = 0; i < monstersOnFloor.length; i++) {
            LinkedList<LocationGenerator> floorLocations =
                    locGenerator.getLocationList().get(i);
            for (LocationGenerator lg : floorLocations) {
                if (lg.getMonster() != null) {
                    monstersOnFloor[i]++;
                }
            }
        }
    }

    @Override
    public void killMonster() {
        monstersOnFloor[currentFloor]--;
    }

    @Override
    public int getCurrentLocationIndex() {
        return currentLocationIndex;
    }

    @Override
    public int getCurrentFloor() {
        return currentFloor;
    }

    @Override
    public int getLastFloorIndex() {
        return LocationMapGenerator.FLOORS - 1;
    }

    @Override
    public int getLastLocationIndex() {
        return LocationMapGenerator.ROOMS - 1;
    }

    @Override
    public String getLocationInfo() {
        StringBuilder locationInfo = new StringBuilder();
        locationInfo.append("Floor = " + currentFloor + ", room = " + currentLocationIndex);
        if (currentLocation.getMonster() != null) {
            locationInfo.append(", monster = " + getLocation().getMonster());
        }
        return locationInfo.toString();
    }

    @Override
    public Item getItem() {
        currentLocation.setChecked(true);
        return currentLocation.getItem();
    }

    @Override
    public Monster getMonster() {
        return currentLocation.getMonster();
    }

    @Override
    public void moveNextLocation() {
        if (hasNextLocation()) {
            currentLocationIndex++;
            currentLocation = getLocation();
        } else {
            System.out.println("Something wrong with moveNext");
        }
    }

    @Override
    public void movePreviousLocation() {
        if (hasPreviousLocation()) {
            currentLocationIndex--;
            currentLocation = getLocation();
        } else {
            System.out.println("Something wrong with movePrevious");
        }
    }

    @Override
    public void moveUpstairs() {
        if (hasUpperFloor()) {
            currentFloor++;
            currentLocationIndex = FIRST_LOCATION;
            currentLocation = getLocation();
        } else {
            System.out.println("cannot moveUpstairs");
        }
    }

    @Override
    public void moveDownstairs() {
        if (hasLowerFloor()) {
            currentFloor--;
            currentLocation = getLocation();
        } else {
            System.out.println("cannot moveDownstairs");
        }
    }

    @Override
    public LocationModel getLocation() {
        return locGenerator.getLocationList().get(currentFloor).get(currentLocationIndex);
    }

    @Override
    public boolean hasUpperFloor() {
        if (currentFloor < getLastFloorIndex()
                && (currentLocationIndex == FIRST_LOCATION
                || currentLocationIndex == getLastLocationIndex())) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    public boolean hasLowerFloor() {
        if (currentFloor > FIRST_FLOOR
                && (currentLocationIndex == FIRST_LOCATION
                || currentLocationIndex == getLastLocationIndex())) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    public boolean hasNextLocation() {
        LinkedList<LocationGenerator> currentFloorLocations =
                locGenerator.getLocationList().get(currentFloor);
        if (currentLocationIndex < (LocationMapGenerator.ROOMS - 1)
                && currentFloorLocations.get(currentLocationIndex + 1) != null) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    public boolean hasPreviousLocation() {
        LinkedList<LocationGenerator> currentFloorLocations =
                locGenerator.getLocationList().get(currentFloor);
        if (currentLocationIndex > 0
                && currentFloorLocations.get(currentLocationIndex - 1) != null) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    public boolean canMoveUp() {
        if (monstersOnFloor[currentFloor] != 0) {
            return false;
        } else {
            return true;
        }
    }

    @Override
    public boolean isChecked() {
        return currentLocation.isChecked();
    }

    @Override
    public boolean hasMonster() {
        if (currentLocation.getMonster() != null
                && currentLocation.getMonster().isLive()) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    public boolean hasStairs() {
        return currentLocation.hasStairs();
    }

    @Override
    public boolean isAnyMonsters() {
        int monstersCount = 0;
        for (int monsters: monstersOnFloor) {
            monstersCount += monsters;
        }
        if (monstersCount == 0)
            return true;
        else
            return false;
    }
}
